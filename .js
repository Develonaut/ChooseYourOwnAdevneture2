/* Author: Created By Ryan McHenry, ryanmmchenry@gmail.com,
 Visit my personal site: www.develonaut.com */
alert("You awake to find yourself in an open field with a lush forest surrounding you?");
alert("You hear a roar in the distance that quickly moves in your direction!");
var endGame = false;

while(true) {
    var user = prompt("The sound doesn't seem to be slowing down, and is getting closer! Do you Run? Hide? or Stay?").toUpperCase();
    switch(user) {
        case "RUN":
            alert("You take off running into the forest!");
            alert("The forrest is thick with trees and dense shrubbery, running is difficult.");
            var machete = prompt("You find a machete laying on the ground, do you pick it up?").toUpperCase();
            if(machete === "YES") {
                alert("You pick the machete and realize it is terribly rusted. It slips out of your hand and cuts you. " +
            "You bleed out in the dense foliage and are never heard of again.");
            } else if(machete === "NO") {
                alert("You leave the machete behind and are able to escape unscathed.");
            }
            endGame = true;
            break;
        case "HIDE" :
            alert("You see some thick foliage near you, and you choose to hide!");
            var breath = prompt("The beast enters the clearing, do you hold your breath?").toUpperCase();
            var phone = prompt("Your phone starts to ring! Do you turn it off?").toUpperCase();
            if(breath === "YES" && phone === "YES") {
                alert("You chuck your phone and the beast is now distracted. He runs in the direction of the phone and you escape safely!");
            } else if (breath === "YES" && phone === "NO") {
                alert("The beast hears your phone and starts to charge you!");
                alert("You pass out from holding your breath and are eaten while unconscious!");
            } else if (breath === "NO" && phone === "NO") {
                alert("You are making to much noise! The beast hears you and you are eaten!");
            } else if (breath === "NO" && phone === "YES") {
                alert("You chuck your phone into the distance, and the beast is now distracted. He runs in the direction of the phone, and you escape safely!");
            }
            endGame = true;
            break;
        case "STAY":
            alert("You choose to stand your ground!");
            var heels = prompt("You notice that you are wearing red heels. Do you click them together?").toUpperCase();
            var count = prompt("Do you count to three?").toUpperCase();
            if (heels === "NO" || count === "NO") {
                alert("You have to do both if you want survive!");
                alert("There isn't enough time to try again and the beast charges you and swallows you whole!");
            } else if (heels ==="YES" && count === "YES") {
                alert("The beast charges you!");
                alert("You click your heels and count to three as fast as you can. You wake up back in Kansas! Unfortunately the dog didn't make it.");
            }
            endGame = true;
            break;
        default:
            alert("Quickly you must choose one of the three!");
            break;
    }
    if(endGame) {
       var playAgain = prompt("Do you wanna play again?").toUpperCase();
            if(playAgain === "NO") {
            break;
            } else {
                endGame = false;
            }
        }
}
